if [ "$(ls -A /config_dir)" ]; then
    config_dir="/config_dir"
else
    config_dir="/etc/transmission-daemon"
fi

transmission-daemon -f -t -u $USERNAME -v $PASSWORD -w /downloads -g $config_dir -p $TRANSMISSION_DAEMON_PORT -P $TRANSMISSION_DAEMON_PEER_PORT
