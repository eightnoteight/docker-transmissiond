FROM ubuntu:16.04
MAINTAINER Srinivas Devaki (mr.eightnoteight@gmail.com)

RUN apt update && \
    apt upgrade -y && \
    apt install transmission-cli transmission-common transmission-daemon -y

COPY app.sh /workshop/app.sh

VOLUME /downloads /config_dir

ENV TRANSMISSION_DAEMON_PORT 9901
ENV TRANSMISSION_DAEMON_PEER_PORT 51413

ENV USERNAME change_to_your_username
ENV PASSWORD change_to_your_password

EXPOSE $TRANSMISSION_DAEMON_PORT $TRANSMISSION_DAEMON_PEER_PORT/tcp $TRANSMISSION_DAEMON_PEER_PORT/udp

CMD ["bash", "/workshop/app.sh"]
